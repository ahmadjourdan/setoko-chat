import styles from '../../scss/modal-custom.scss';
import {
    createDivEl
} from '../utils';
import {
    Spinner
} from './Spinner';
import {
    body
} from '../const';

class ModalProduct {
    constructor({
        title,
        description,
        submitText,
        dataProduct
    }) {
        this.contentElement = null;
        this.cancelHandler = null;
        this.submitHandler = null;
        this.element = this._create({
            title,
            description,
            submitText,
            dataProduct
        });
    }

    _create({
        title,
        description,
        submitText,
        dataProduct
    }) {
        const root = createDivEl({
            className: styles['modal-root']
        });
        const modal = createDivEl({
            className: styles['modal-body']
        });
        root.appendChild(modal);

        const titleText = createDivEl({
            className: styles['modal-title'],
            content: title
        });
        modal.appendChild(titleText);

        const desc = createDivEl({
            className: styles['modal-desc'],
            content: description
        });
        modal.appendChild(desc);
        // Card Products 
        let cardProduct = `<div class="row">`;
        dataProduct.forEach(function (data, index) {
            cardProduct += `<div class="user-item">
                        <div class="user-info">
                            <div>
                                <img src="${data.medias[0].url}" style="width:60px; height:autos"/>
                            </div> 
                            <div class="user-nickname">${data.name}
                                </br>
                                <span style="font-size:12px; color:grey;">${data.description}</span>
                            <div>
                            </div>
                            </div>
                        </div>
                        <div class="user-state">
                            <input type="checkbox" name="product-checkbox" value="${data.code}"/>
                            <div class="user-time"></div>
                        </div>
                    </div>`;
        });
        cardProduct += `</div>`;
        // End Card Products
        this.contentElement = createDivEl({
            className: styles['modal-content']
        });
        this.contentElement.innerHTML = cardProduct;
        modal.appendChild(this.contentElement);

        const bottom = createDivEl({
            className: styles['modal-bottom']
        });
        modal.appendChild(bottom);
        const cancel = createDivEl({
            className: styles['modal-cancel'],
            content: 'CANCEL'
        });
        cancel.addEventListener('click', () => {
            if (this.cancelHandler) {
                this.cancelHandler();
            }
            this.close();
        });
        bottom.appendChild(cancel);
        const submit = createDivEl({
            className: styles['modal-submit'],
            content: submitText
        });
        submit.addEventListener('click', () => {
            // Spinner.start(modal);
            // if (this.submitHandler) {
            //     this.submitHandler();
            // }
            var checkboxes = document.querySelectorAll('input[name="product-checkbox"]:checked'),
                values = [];
            Array.prototype.forEach.call(checkboxes, function (el) {
                values.push(el.value);
            });
            console.log(values);
        });
        bottom.appendChild(submit);

        return root;
    }

    close() {
        if (body.contains(this.element)) {
            body.removeChild(this.element);
        }
    }

    render() {
        if (!body.querySelector(`.${styles['modal-root']}`)) {
            body.appendChild(this.element);
        }
    }
}

export {
    ModalProduct
};