export const APP_ID = '9D5C3659-8EBE-4163-BA09-B7498F61E2B6';
export const USER_ID = 'user1';
export const NICKNAME = 'User1';
export const DISPLAY_NONE = 'none';
export const DISPLAY_BLOCK = 'block';
export const DISPLAY_FLEX = 'flex';
export const ACTIVE_CLASSNAME = 'active';
export const KEY_ENTER = 13;
export const FILE_ID = 'attach_file_id';
export const UPDATE_INTERVAL_TIME = 5 * 1000;
export const COLOR_RED = '#DC5960';
export const MESSAGE_REQ_ID = 'reqId';
export const OPEN_CHANNEL_SEARCH_URL = 'search_open_channel';

export const body = document.querySelector('body');